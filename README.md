# pihole-and-forget

A Pihole setup with remote access for maintenance so you can give the gift of ad-free-ness to others

## Goal
To provide the goodness of Pihole to less tech-savy people in the form of a Raspberry Pi that just get's plugged into their network.

On top of Pihole, this setup also relies on DuckDNS and OpenVPN so that the system can be fixed remotely if need be.

## Setup
### SSH
- enable SSH access on the Raspberry Pi
- copy SSH key over: `ssh-copy-id -i <key_file>.pub pi@<host>`
- disable SSH password login by setting
    ```
    PasswordAuthentication no
    UsePAM no
    PermitRootLogin no
    ```
    in `/etc/ssh/sshd_config`
### Docker
- install docker: `curl -sSL https://get.docker.com | sh`
- add `pi` as a docker user (optional): `sudo usermod -aG docker pi`
- install `docker-compose`:
    ```bash
    apt install python-pip  # may already be installed
    pip install docker-compose
    ```

Now is probably a good time to reboot.

## Running everything
First, clone this repository to your Pi.

### Filling in Docker environment variables
1. Copy and rename `.env_template` to `.env` and replace values after each `=` with the following:
1. Set user and group IDs to the one that you want all docker containers to write with. Normally this would just be the `pi` user with UID 1000 and GID 1000. To be sure, run `id`.
1. Decide on which DNS servers you want your services to be using since you don't want your remote access setup going through your new DNS server. The defaults for OpenDNS and Google should be fine
1. Assuming the external IP address of the network that you're freeing from ads cannot be guaranteed to be static, we need a dynamic way of finding it. DuckDNS is great for this. Go to duckdns.org, log in/create an account and register a new subdomain. This goes in `SUBDOMAINS`. Next, get your token (visible a bit higher up on the page) and paste it into `DUCKDNS_TOKEN`
1. Set `SERVER_IP` to the Pi's IP address. This will have to be changed to the new address once the whole thing is deployed in it's new network.
1. Set a password for Pihole's admin console.
1. Add a folder path that you want your Pihole's config files to be saved to. Make sure it's an absolute path (starts with `/` and not something like `~`).

### Installing OpenVPN
Just follow pivpn.io. It's probably okay to leave everything at default during the installation with a few exceptions:
- If you want to use a more obscure port, you can always change the VPN port later on in the router. Just make sure to edit the `.ovpn` file that's generated later accordingly.
- Set the DuckDNS address as DNS entry.

After the suggersted reboot, run `pivpn add` and then copy over the generated profile via `scp pi@<internal_ip>:~/ovpns/<name_selected_just_now>.ovpn .` If you plan on forwarding VPN access via a different port, edit the port number in this file.

### Starting Docker containers
Assuming you're inside the repository folder, run `docker-compose up -d`. Wait for all images to be pulled and containers to be started.

If need be, check logs with `docker-compose logs -tf --tail="50"`. You can append the container name to that command.

### Setting up Pihole
Go to `http://raspberrypi/admin/` (or the corresponding IP address if that's not the right hostname). Not much to do here, actually. Maybe add some more blocklists (for example frome [here](https://firebog.net/)) or just copy lists over from your own setup, assuming you're already using Pihole yourself.

You might also want to check out the whitelist section on the page linked above in order to ensure you're not breaking too much. Alternatively or on top of that, check out [anudeepND's curated whitelists](https://github.com/anudeepND/whitelist) and follow the steps in the README by bashing into the pihole container (`docker exec -it pihole bash` and then follow the steps). The clone of the repo will be lost on container restarts but the whitelists are written to the mounted volume and will persist.

You might want to make it easier for the end-user to whitelist some domains by showing the Pihole block page (from where you can whitelist if you know the admin password). To do so, follow [these instructions](https://docs.pi-hole.net/ftldns/blockingmode/). The config file will be in the `PIHOLE_DATA_DIR` path that you set in the `.env` file. To issue the kill command listed at the top of the page, simply execute `docker exec -it pihole killall -SIGHUP pihole-FTL`.

### Automatic Updates
Watchtower takes care of automatically updating all containers. The only thing left to do is cleaning up Docker caches so you don't run out of disk space.

Using `crontab -e`, this can look something like this:
```
00 04 * * *  docker system prune -af
```

## Deployment
Disclaimer: I haven't done these steps myself yet. Still waiting for Christmas to roll around.

So far, I'm assuming the Pi has been set up not at its final location. To get things running in the new network, a few more steps are required:
1. Plug the Pi in in the new network
1. Assuing the network setup is not exactly the same: Fix the static IP that was set during OpenVPN setup by changing the last few lines in `/etc/dhcpcd.conf`
1. Set the `SERVER_IP` variable to the new IP.
1. Enable port forwarding in the router from any external port to 1194 on the Pi's new IP.
1. Set the router's DNS server to the Pi's IP.